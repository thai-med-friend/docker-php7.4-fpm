FROM php:7.4-fpm-alpine

RUN set -xe \
  # update repository
  && apk update \
  && apk upgrade \
  # mysqli
  && apk add --no-cache git wget supervisor \
  && docker-php-ext-install mysqli pdo pdo_mysql \
  # gd
  && apk add --update --no-cache \
  freetype-dev freetype libpng-dev libpng libjpeg-turbo-dev libjpeg-turbo \
  libmcrypt-dev libmcrypt libwebp-dev libwebp \
  && docker-php-ext-configure gd \
  --with-freetype=/usr/include/ \
  --with-jpeg=/usr/include/ \
  --with-webp=/usr/include/ \
  && NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) \
  && docker-php-ext-install -j${NPROC} gd \
  && docker-php-ext-configure opcache --enable-opcache \
  && docker-php-ext-install opcache \
  && apk add --no-cache --virtual .phpize-deps $PHPIZE_DEPS imagemagick-dev libtool \
  && export CFLAGS="$PHP_CFLAGS" CPPFLAGS="$PHP_CPPFLAGS" LDFLAGS="$PHP_LDFLAGS" \
  && pecl install imagick-3.4.3 \
  && docker-php-ext-enable imagick \
  && apk add --no-cache --virtual .imagick-runtime-deps imagemagick \
  # clear
  && apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev libmcrypt-dev libwebp-dev .phpize-deps \
  && rm -rf /tmp/* /var/cache/apk/*

RUN mkdir -p /var/log/cron && mkdir -m 0644 -p /var/spool/cron/crontabs && touch /var/log/cron/cron.log && mkdir -m 0644 -p /etc/cron.d

# composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN apk add --update nodejs
RUN apk add yarn
RUN apk add bash
RUN apk add ffmpeg
RUN apk add shellcheck
RUN apk add --no-cache file
RUN apk add imagemagick

COPY php.ini /usr/local/etc/php/

COPY opcache.ini /usr/local/etc/php/conf.d/

# COPY init.sh /usr/local/bin/init.sh

WORKDIR /var/www/html

# RUN chown -R root:root /usr/local/bin/init.sh \
#   && chmod u+x /usr/local/bin/init.sh

# CMD ["sh", "/usr/local/bin/init.sh"]
